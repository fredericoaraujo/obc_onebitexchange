# Projeto OneBitExchange

Esse é um projeto de aprendizagem de Ruby on Rails que busca a conversão de moedas consumindo serviço rest.

Para esse projeto está sendo utilizado o serviço [Currency Data Feed](https://currencydatafeed.com).

Tecnologias empregadas:

- Ruby on Rails;
- RSpec;
- rest-api;

